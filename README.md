# WebGame menu

Tato semestralni prace je slozena ze 2 casti.
1.  casti je web-game menu, kde si hrac cte novinky, spravuje svuj ucet (popripade resitruje si ucet), muze se podivat na tutorialy
    a vybírá si hry, které chce hrát
2.  Je primo implementovana jedna hra s nazvem mole-hammers, kde hraci s pomoci klaves bouchaji krtky, vyhrava ten hrac, keremu se jich podari strefit nejvice.

Struktura kodu je takovato:
#Web-game menu
-css (css vlsatnosti pro web-game menu)
- games_list (zde se naleza repozitar jednotlivych her, kazda je ulozena v samostatne slozce, navíc je zde JSON s metadaty o hrach)
- icons (pouzivane ikony)
- images (slozka pouzivana pro obrazky na strance, nyni pouze pro tutorial)
- js (javascripty pouzivane pro web-game menu)
- index.html (hlavni stranky)
- README.md (tento soubor)
- registration (stranka pro registraci)

#mole-hammers
- audio (zvuky ve hre)
- css (css soubory pro hru)
- js (iniciacni a objektove funkce pro spravny chod hry)
- svg (svg ikony pro hru)
- index.html (staticka stranka se hrou)
- README.md (popis hry)




Zde budu vypisovat povinne body (z tabulky na pozadavky semestralni praci) a k ni budu prirazovat casti aplikace (kodu), kde se jednotlive veci nachazeji.

#=========== Dokumentace ===============================================
cíl projektu, postup, popis funkčnosti, komentáře ve zdrojovém kódu ==> časti kódu jsou popsané (všechny metody co dělají) a to jak u web=game menu, tak i                                                                          u mole-hammers
# ==================> CELKEM BODU: 1
# ==================== HTML 5 ====================
Validita - Validní použití HTML5 doctype ==> ano
Validita - "Fungující v moderních prohlíčečích v posledních vývojových verzích (Chrome, Firefox, Edge, Opera)" ==> ano
Semantické značky - "správné použití sémantických značek (section, article, nav, aside, ...)" ==> ano všude
Grafika - SVG / Canvas	==> ano u mole-hammers je pouzity canvas
Média - Audio/Video	==> ano u mole-hammers hraje muzika
Formulářové prvky - Validace, typy, placeholder, autofocus ==> u formulare pro registraci funguje validace a typy
Offline aplikace - "využití možnosti fungování stránky bez Internetového připojení ==> ano lze stranku vyuzit i bez internetu
# ==================> CELKEM BODU: 10
# ==================== CSS ====================
Pokročilé selektory	- použití pokročilých pseudotříd a kombinátorů ==> ano u registracniho formulare, v souboru nav_menu.css u sekce menu-list ad.
Vendor prefixy	==> nemám důvod je pouzivat
CSS3 transformace 2D/3D ==> ano v halvicce u web-game menu je 3D menu	
CSS3 transitions/animations	==> ano v halvicce u web-game menu je 3D menu, v mole_hammer ad.
Media queries - stránky fungují i na mobilních zařízeních i jiných (tedy nerozpadají se) ==> cast web-game menu je plne responzivni, mole-hammers je    responzivni castecne a to tak, ze se velikost hernoho okna pri prvnim spusteni nastavi na okno uzivatele
# ==================> CELKEM BODU: 7 (+ 1 za vendor prefixy)
# ==================== JAVASCRIPT ====================
OOP přístup - prototypová dědičnost, její využití, jmenné prostory ==> ano vsude (zvlaste u mole=hammers)
Použití JS frameworku či knihovny - použití a pochopení frameworku či knihovny jQuery, React, Vue .. ==> bohuzel, nechtel jsem tam davat neco dalsiho
Použití pokročilých JS API	využití pokročilých API (File API, Geolocation, Drag & Drop, LocalStorage, Sockety, ...) ==> ano, u registrace drag and drop pro fotku, nacteni fotky pres FileReader, ulozeni informaci do LocalSotorage ad.
Funkční historie - posun tlačítky zpět/vpřed prohlížeče - pokud to vyplývá z funkcionatilty (History API) ==> ano hlavni stranky (home, tutorial, games, about) jsou resene pres History API (viz javascript, soubor nav_menu.js)
Ovládání medií - použití Média API (video, zvuk), přehrávání z JS ==> ano, u hry mole-hammers je pouzit zvuk
Offline aplikace - využití JS API pro zjišťování stavu ==> na pravém místě v hlavicce menu (nalevo od uzivatelske fotky) je kolecko znazornujici stav stranek (pokud je zelene, je uzivatel online, pokud je cervene je uzivatel offline) 
JS práce se SVG - události, tvorba, úpravy ==> ano, u hry mole-hammers jsou všechny obrázky v SVG a u web-game menu v sekci her nad jednotlivými hry je javascriptem vymodelovaný SVG play button, viz sekce js/games.js
# ==================> CELKEM BODU: 11
# ==================== OSTATNI ==================== 
Kompletnost řešení	==> vase hodnoceni
Estetické zpracovani ==>vase hodnoceni
# ==================> CELKEM BODU: ? (0 - 5)

#==== BODU CELKEM ZA VSECHNY POLOZKY: 29 (+ vase body ze sekce OSTATNI)