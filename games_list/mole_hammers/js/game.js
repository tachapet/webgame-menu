import Controller from './controller.js';
import Display from './display.js';
import Hammer from './objects/hammer.js';
import ControllKey from './objects/ControllKey.js';
import Mole from './objects/mole.js';

//GAMESTATE, stavy do ktere se muze hra dostat
const GAMESTATE = {
  PAUSED: 0,
  RUNNING: 1,
  MENU: 2,
  GAMEOVER: 3,
  NEWLEVEL: 4
};

// Trida pro vytvoreni hry, Funguje na principu Model/View/controller 
export default class Game {
    //Konstruktor pro nasetovani Modelu, View a Controlleru a veškerých potřebných assetů pro hru (včetně hernich objektů)
    constructor(gameWidth, gameHeight) {

      this.gameWidth = gameWidth;
      this.gameHeight = gameHeight;
      this.gamestate = GAMESTATE.MENU;
      this.started = false;

      this.scorePlayer1 = 0;
      this.scorePlayer2 = 0;



      this.currentTime = 0;
      this.timeGameStarted = 0;
      this.timeEndGame = 20;
      this.timeCurrentTime = 0;




      this.countdownTimer;

      this.numberOfplayers = 0;

      this.menu = document.querySelector("#main");
      this.gameOverSelector = document.querySelector("#endgame");

      this.display = new Display(document.getElementById("canvas"),this.gameWidth,this.gameHeight);

      this.controller = new Controller(this.display,this);

      this.hammer1 = new Hammer(this.gameWidth,this.gameHeight,document.querySelector("#hammer-player-1"));
      this.hammer2 = new Hammer(this.gameWidth,this.gameHeight,document.querySelector("#hammer-player-2"));

      this.WKey = new ControllKey(this.gameWidth,this.gameHeight,document.querySelector("#key-player-1"));
      this.arrowKey = new ControllKey(this.gameWidth,this.gameHeight,document.querySelector("#key-player-2"));

      this.mole = new Mole(this.gameWidth,this.gameHeight,document.querySelector("#mole"));


    
    }
  
    //Nastaveni poctu hracu
    setNumberOfPlayers(number){
      this.numberOfplayers = number;
    }

    //funkce pro spusteni hry
    start() {
    
      this.display.show(this.menu);   
      this.gamestate = GAMESTATE.MENU; 
     
      return this.started;
      
    }

    //funkce pro nastaveni nove hry
    gameStarted(){
      document.querySelectorAll(".result").forEach(result =>{
        this.display.hide(result);
      })
      this.gamestate = GAMESTATE.RUNNING;
      this.timeGameStarted = this.currentTime;
      this.scorePlayer1 = 0;
      this.scorePlayer2 = 0;
      this.timeCurrentTime = 0;
      
    }
    
    //vyresetoani hry (nastaveni prvotního stavu)
    gameReset(){
      this.gamestate = GAMESTATE.MENU;
    }
  
    // funkce update (zaktualizovani stavu, ziskani soucasneho casu, updatovani jednotlivych hernich objektu)
    update(currentTime) {
      
      
      this.currentTime = currentTime;

      if (
        this.gamestate === GAMESTATE.PAUSED ||
        this.gamestate === GAMESTATE.MENU ||
        this.gamestate === GAMESTATE.GAMEOVER
      ){
        return;}
      
       if(this.timeCurrentTime >= this.timeEndGame){
         
        this.gamestate = GAMESTATE.GAMEOVER;
      } 


    

    
      
        this.controller.update();
        this.hammer1.update();
        this.hammer2.update();
        this.mole.update(currentTime);
    
    
  
   

    }
  
    // Funkce pro vykreslovani hry 
    draw() {


      // Pri stavu PAUSED nebo MENU se nic nevykresluje navic
      if (
        this.gamestate === GAMESTATE.PAUSED ||
        this.gamestate === GAMESTATE.MENU
      ){
        return;}

      

        //Prigame overu se vykresli kdo vyhral
        if (this.gamestate === GAMESTATE.GAMEOVER) {

          let player1wins = document.querySelector("#player1wins");
          let player2wins = document.querySelector("#player2wins");
          let tie = document.querySelector("#tie");
          this.display.clearCanvas();
          let winner = "";
          if(this.scorePlayer1 > this.scorePlayer2){
            /* winner = "PLAYER 1"; */
            this.display.show(player1wins);
          }
          else if(this.scorePlayer1 < this.scorePlayer2){
           /*  winner = "PLAYER 2"; */
            this.display.show(player2wins);
          }
          else{
            /* winner = "TIE"; */
            this.display.show(tie);
          }
          this.display.show(this.gameOverSelector);
          /* this.display.showEndScreen(this.gameOverSelector, winner); */
          return;
        }

      // V ostatnich pripadech se vykresli herni objekty na spravnych pozicich

        this.display.clearCanvas();

      this.display.rotateHammer(this.hammer1);
      this.display.rotateHammer(this.hammer2);


       this.display.drawText(this.convertSeconds(), this.gameWidth/2,this.gameHeight/10);

    // Zmena pri vykreslovani pri jendom hraci a vice hracich
    if(this.numberOfplayers ==2){
      this.display.drawGameObject(this.arrowKey);
      this.display.drawGameObject(this.WKey);
    }else{
      this.display.drawGameObject(this.WKey);
    }
      this.display.drawGameObject(this.mole);
    

      this.display.drawText(this.scorePlayer1, (this.gameWidth/5), (this.gameHeight - this.gameHeight/10));
      this.display.drawText(this.scorePlayer2, (this.gameWidth - this.gameWidth/5), (this.gameHeight - this.gameHeight/10)); 





    }

    //Funkce pro konvertovani casu
    convertSeconds(){
       let sec = Math.round(this.currentTime/1000);
       this.timeCurrentTime = sec - Math.round(this.timeGameStarted/1000);
       return this.timeEndGame - this.timeCurrentTime;
     }



  }
  