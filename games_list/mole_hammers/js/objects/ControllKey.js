//**Trida, ktera je urcena pro dane talcitko/a pro hru (v teto hre se jenda o tacitka S a ArrowDown, kdy každé tlačítko má vlastní instanci) */
export default class ControllKey{
    // vytvoreni tlacitka a nastaveni jeho pozice
    constructor(gameWidth,gameHeight, key){
        this.CANVAS_WIDTH = gameWidth;
        this.CANVAS_HEIGHT = gameHeight;
        this.key = key;
        
        this.height = this.CANVAS_HEIGHT - this.CANVAS_HEIGHT/1.5 ; 


        this.width = this.CANVAS_WIDTH /7;
        this.y = this.CANVAS_HEIGHT - this.height -10;

        if(this.key.dataset.type == 0){
            this.x = this.CANVAS_WIDTH / 50 ;
        }
        else{
            this.x = this.CANVAS_WIDTH - ( this.CANVAS_WIDTH /50 ) - this.width;
        }

    }

    getImage(){
        return this.key;
    }

    getX(){
        return this.x;
    }
    
    getY(){
        return this.y;
    }
    getWidth(){
        return this.width;
    }
    getHeight(){
        return this.height;
    }


}