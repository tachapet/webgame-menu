//**Trida , ktera reprezentuje "krtka" ve hre */
export default class Mole {
  //Vytvoreni krtka a nastaveni jeho parametru
  constructor (gameWidth, gameHeight, mole) {
    this.CANVAS_WIDTH = gameWidth
    this.CANVAS_HEIGHT = gameHeight
    this.source = mole
    this.speed = 20
    this.hit = false
    this.hitable = false
    this.itsTime = false
    this.lastTimeShwon = 2000

    this.height = this.CANVAS_HEIGHT - this.CANVAS_HEIGHT / 10
    this.width = this.CANVAS_WIDTH / 2
    this.x = this.CANVAS_WIDTH / 2 - this.width / 2 + this.width / 100
    this.y = this.CANVAS_HEIGHT // must go -y
  }

  // Update metoda, ktera ridi krtka, jedna se o funkci ktera v nejaky nahodny cas zacne krtka vystrkavat nahoru,
  // a kdyz uz je dostatecne vysoko, nastavi se jeho promenna hittable na true (krtka nyni lze zasahnout)
  // Pohyb krtka funguje na klasickem pridavani hodnoty x (zmena polohy na canvasu)
  update (currentTime) {
    if (
      currentTime >
      this.lastTimeShwon + (Math.floor(Math.random() * 20000) + 1000)
    ) {
      this.itsTime = true
      this.lastTimeShwon += currentTime - this.lastTimeShwon
    }

    if (!this.hit && this.itsTime) {
      if (this.y < this.CANVAS_HEIGHT - this.CANVAS_HEIGHT / 2) {
        this.hitable = true
        return
      }

      this.y -= this.speed
    } else {
      if (this.itsTime) {
        this.y += this.speed * 4

        if (this.y > this.CANVAS_HEIGHT) {
          this.y = this.CANVAS_HEIGHT
          this.hitable = false
          this.itsTime = false
          this.hit = false
          this.lastTimeShwon = currentTime
        }

        /*  this.y += this.speed*4; */
      } else {
      }
    }
  }

  getImage () {
    return this.source
  }

  getX () {
    return this.x
  }

  getY () {
    return this.y
  }
  getWidth () {
    return this.width
  }
  getHeight () {
    return this.height
  }
}
