//**trida reprezentujici kladivo ve hre */
export default class Hammer{
    // Kontruktor pro nastaveni kladiva a jeho pozice
    constructor(gameWidth,gameHeight, hammer){
        this.CANVAS_WIDTH = gameWidth;
        this.CANVAS_HEIGHT = gameHeight;
        this.source = hammer;
        this.velocity_x = 1;
        this.height = this.CANVAS_HEIGHT - this.CANVAS_HEIGHT / 8 -100;
        this.width = this.CANVAS_WIDTH /5 - 10;
        this.y = 10;
        this.pressedKey = false;
        this.angle = 0;
        this.orientation = hammer.dataset.position;
        this.speed = 15;
        this.releaseKey =true;
        

        if(this.source.dataset.position == 1){
            this.x = this.CANVAS_WIDTH / 50 ;
            this.rotatePositionX = (this.x+2*this.width/3) - (this.x+2*this.width/3)/3;
            this.rotatePositionY = this.y+ this.height -(this.y+ this.height)/7;

            this.rotateDrawImageX = -2*this.width/3+(this.x+2*this.width/3)/3;
            this.rotateDrawImageY = -this.height+(this.y+ this.height)/7;

        }
        else{
            this.x = this.CANVAS_WIDTH - ( this.CANVAS_WIDTH /50 ) - this.width;
            this.rotatePositionX = (this.x+2*this.width/3) - (this.x+2*this.width/3)/8 +125;
            this.rotatePositionY = this.y+ this.height -(this.y+ this.height)/8 ;

            this.rotateDrawImageX = -2* this.width/3+(this.x+2*this.width/3)/8 -125;
            this.rotateDrawImageY = -this.height+(this.y+ this.height)/8 ;
        }


    }

    // Funkce pro nastaveni, jestli stisknuto tlacitko pro uder kladiva, ci nikoliv
    keyUp(){
      
        this.releaseKey = true;
        this.pressedKey = false;
       
    }
    
    // Funkce pro kontrolu jestli je tlacitko pro kladivo stisknute, ci nikoliv
    /*
     * 0 for down
     * 1 for up
    */
    keyDown(){
       if(this.releaseKey){ 
       this.pressedKey = true;
    }
       
    }

    // Funkce update, ktera meni v zavislosti na stisku tlacitka uhel kladiva
    update() {
        
        if(this.pressedKey){
            this.releaseKey = false;
            
            if(this.angle > 50){
                 this.pressedKey = false;
            }else{      
               
                this.y += this.speed * this.orientation;
                this.angle += this.speed % 360 ;
            }
        }
        else{
            if(this.angle > 0){
                            
                this.y -= this.speed * this.orientation;
                this.angle -= this.speed % 360 ;

            }


        }

      }

    getImage(){
        return this.source;
    }

    getX(){
        return this.x;
    }
    
    getY(){
        return this.y;
    }
    getWidth(){
        return this.width;
    }
    getHeight(){
        return this.height;
    }



}