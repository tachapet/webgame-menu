//**Iniciativni script pro spusteni hry */


import Game from "./game.js";

/* const GAME_WIDTH = 800;
const GAME_HEIGHT = 600; 

640 X 480
800 X 600
1280 X 720

*/

let pointWindow = (window.innerHeight*4/5)/9;

//Nastaveni velikosti okna
const GAME_WIDTH = pointWindow*16 /* 1280 */;
const GAME_HEIGHT =  pointWindow*9 /* 720 */;

//Vytvoreni nove instance Hry
let game = new Game(GAME_WIDTH, GAME_HEIGHT);

// Gameloop (vzasade se jedna jen o to, ze se vola v časove smycce funkce draw() a funkce update())
let lastTime = 0; 
let gameStarted = false;
gameStarted = game.start();

function gameLoop(timestamp) {
 
  let deltaTime = timestamp - lastTime;

  lastTime = timestamp;
  

  game.update(timestamp);
  game.draw();

  requestAnimationFrame(gameLoop);
}


requestAnimationFrame(gameLoop);