//** Trida pro rizeni udalosti ve hre (controller) */
export default class Controller {
  // diskani referenci a zavolani metod pro nasetovani udalosti
  constructor (display, game) {
    this.display = display
    this.game = game

    this.menu = document.querySelector('#main')
    this.canvas = document.querySelector('canvas')
    this.gameWrapper = document.querySelector('#game')
    this.endgameWrapper = document.querySelector('#endgame');
    this.musicSpeaker = document.querySelector("#music");
    this.speakerIcon = document.querySelector('.speakerIcon');
    this.speakerIconDisable = document.querySelector('.speakerIconDisable');

    this.menuHandler()
    this.generalHandler()
  }

  // vytvoreni posluchace na menu hry
  menuHandler () {
    document.querySelectorAll('.play').forEach(button => {
      button.addEventListener('click', e => {
        this.game.setNumberOfPlayers(e.target.dataset.players)
        this.game.gameStarted()
        this.display.hide(this.menu)
        this.display.show(this.gameWrapper)
      })
    })

    document.querySelector('.again').addEventListener(
      'click', e =>
      {
        this.game.gamestate = 2;
        this.display.show(this.menu);
        this.display.hide(this.endgameWrapper);

      }

    )

    //Speaker handler     
    document.querySelector('#speaker').addEventListener('click', e => {
     
      if(e.target.dataset.play == "true"){
        e.target.dataset.play = false;
        e.target.src = "./svg/speakerIconDisable.svg";
        this.musicSpeaker.pause();
        return;
      }
      if(e.target.dataset.play == "false"){
        e.target.dataset.play = true;
        e.target.src = "./svg/speakerIcon.svg";
        this.musicSpeaker.play();
      }

    });




  }

  // Hlavni posluchac behem hry
  generalHandler () {
    // Handler pro stisknuti klavesy Escape, s nebo ArrowDown
    window.addEventListener('keydown', e => {
      // Escape handler
      if (e.key == 'Escape') {
        this.display.show(this.menu)
        this.display.hide(this.gameWrapper)
        this.display.hide(this.endgameWrapper)
        this.game.gameReset();
      }
      // s handler
      if (e.key == 's') {
        if (this.game.hammer1.pressedKey == false) {
          this.game.hammer1.keyDown()
        }
      }
      //ArrowDown handler
      if (e.key == 'ArrowDown' && this.game.numberOfplayers == 2) {
        if (this.game.hammer2.pressedKey == false) {
          this.game.hammer2.keyDown()
        }
      }

     
    })
    // handler pro zvednuti klavesy s a ArrowDown
    window.addEventListener('keyup', e => {
      // Escape Handler
      if (e.key == 's') {
       
        this.game.hammer1.keyUp()
      }

      if (e.key == 'ArrowDown'  && this.game.numberOfplayers == 2) {
        this.game.hammer2.keyUp()

      }
    })

  
  }

  // Funkce pro updatovani game objektu a vytvoreni jednoducheho collision efektu 
  //(fungujici na principu: pokud je krtka mozno bouchnout - je hittable, a uhel kladiva je vice nez 50%, zanmená to uspech a pripise se bod danemu kladivvu)
  update () {
    if (
      this.game.hammer1.angle >= 50 &&
      this.game.mole.hitable == true &&
      this.game.mole.hit == false
    ) {
      this.game.mole.hit = true
      this.game.scorePlayer1++;
    }
    if (
      this.game.hammer2.angle >= 50 &&
      this.game.mole.hitable == true &&
      this.game.mole.hit == false
    ) {
      this.game.mole.hit = true
      this.game.scorePlayer2++;
    }
  }
}
