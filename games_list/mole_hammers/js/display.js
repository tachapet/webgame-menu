//** Trida pro kresleni do canvasu (View) */

export default class Display {
  // Vytvořeni canvasu na urcite rozmery
  constructor (canvas, gameWidth, gameHeight) {
    (this.buffer = document.createElement('canvas').getContext('2d')),
    (this.context = canvas.getContext('2d'));
    (this.RAD = Math.PI / 180),(this.CANVAS_WIDTH = gameWidth);
    this.CANVAS_HEIGHT = gameHeight;

    this.context.canvas.width = this.CANVAS_WIDTH - 5
    this.context.canvas.height = this.CANVAS_HEIGHT - 5
  }

  // Funkce, která vymaze vse, co se nachazi na canvasu
  update () {
    this.context.clearRect(0, 0, CANVAS_WIDTH, CANVAS_HEIGHT)
  }

  // Funkce, ktera vykresli objekt (ktery prijme) na pozici X a Y
  drawGameObject (object) {


    this.context.drawImage(
      object.getImage(),
      object.getX(),
      object.getY(),
      object.getWidth(),
      object.getHeight()
    )
  }

  // Funkce, ktera otoci kladivo o urcity uhel
  rotateHammer (hammer) {
    this.context.save() // move to point
      this.context.translate(
      hammer.rotatePositionX,
      hammer.rotatePositionY
    )
    this.context.rotate(hammer.angle * hammer.orientation * this.RAD) // rotate around that point
    this.context.drawImage(
      hammer.getImage(),
      hammer.rotateDrawImageX,
      hammer.rotateDrawImageY,
      hammer.getWidth(),
      hammer.getHeight()
    )

    this.context.restore() // restore to initial coordinates
  }

  // Fuknce, ktera vykresli text (ktery prijme) na souradice x a y
  drawText( text, x, y ){
    this.context.font = "30px Comic Sans MS";
    this.context.fillStyle = "red";
    this.context.fillText(text,x,y); 
  }

   // Funkce, která vymaze vse, co se nachazi na canvasu
  clearCanvas () {
    this.context.clearRect(0, 0, this.CANVAS_WIDTH, this.CANVAS_HEIGHT)
  }

  // Hide input element
  hide (el) {
    el.style.display = 'none'
  }

  // Show input element
  show (el) {
    el.style.display = 'block'
  }

  // Zobrazeni konecne sekce s vyhercem
  showEndScreen (el, winner) {
    el.style.display = 'block';
    const winnerText = el.querySelector("#endResult");
    winnerText.innerHTML = winner
  }
}
