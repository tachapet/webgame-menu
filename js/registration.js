//** Trida, ktera se stara o vlidaci, zobrayeni a odelsani registrace uzivatele */

class Registration {
    /** Kontruktor pro nastavení reference na formular a dale nastaveni eventu. */
  constructor () {
    this.fileList

    this.dropZone = document.getElementById('fileDropZone')
    this.dropZone.addEventListener('dragover', this.dragOver.bind(this), false)
    this.dropZone.addEventListener('dragleave', this.dragLeave.bind(this), false)
    this.dropZone.addEventListener('drop', this.dropFunction.bind(this), false)
    this.obsah = localStorage.getItem('about')

    // SUBMIT
    const submit = document
      .querySelector('#input-submit')
      .addEventListener('click', this.validateForm.bind(this), false)
  }

   //** funkce, ktera precte prvni fotku v poli fileList */
   readFiles() {
    this.loadAsUrl(this.fileList[0])
  }

  //** funkce pro pusteni obrazku nad dropzonenou (funkce zavola funkci readFiles s daty, ktere jsou ulozene v localstorage) */
  dropFunction (e) {
    e.preventDefault()
    e.stopPropagation();
    this.fileList = e.dataTransfer.files
    this.dropZone.classList.remove('dragover')
    this.readFiles(this.fileList);
  }

  //** funkce pro pretahnuti obrazku nad dropzonenou (funkce pouze nastavi jinou classu na el. dropzone pro lepsi interakci s uzivatelem) */
  dragOver (e) {
    e.stopPropagation()
    e.preventDefault()
    this.dropZone.classList.add('dragover');
    e.dataTransfer.dropEffect = 'copy'
  }

  
  //** funkce pro opusteni obrazku nad dropzonenou (funkce pouze nastavi jinou classu na el. dropzone pro lepsi interakci s uzivatelem) */
  dragLeave (e) {
    e.stopPropagation()
    e.preventDefault()
    this.dropZone.classList.remove('dragover')
    e.dataTransfer.dropEffect = 'copy'
  }

 

  //** funkce, ktera nacte fotku jako url a nastavi ji k obrazku */
  loadAsUrl (theFile) {
    var reader = new FileReader()

    reader.onload = function (loadedEvent) {
      var image = document.getElementById('theAvatarImage')
      image.setAttribute('src', loadedEvent.target.result)
    }

    reader.readAsDataURL(theFile)
  }

  checkInputs(nickname,email){
    let correctName = 1;
    let correctEmail = 1;

    if (nickname.value == '') {
      nickname.classList.add("error");
      nickname.nextElementSibling.style.display = "block";
      correctName = 0;
    }else{
      nickname.classList.remove("error");
      nickname.nextElementSibling.style.display = "none";
      correctName = 1;
    }
    if(email.value == ''){
      email.classList.add("error");
      email.nextElementSibling.style.display = "block";
      correctEmail = 0;
    }else{
      email.classList.remove("error");
      email.nextElementSibling.style.display = "none";
      correctEmail = 1;
    }
    return correctName * correctEmail;
  }

    //**  funkce, ktera zvaliduje formular (zzobrazi chybove hlasky), nastetuje localstorage daty z registrace a presmeruje aktualni stranku na stranku index.html*/
  validateForm (e) {
    const nickname = document.getElementById('input-nickname')
    const email = document.getElementById('input-email')
    const about = document.getElementById('input-about')
    const avatar = document.getElementById('theAvatarImage')

    e.preventDefault();

    if(this.checkInputs(nickname,email)) {
      localStorage.setItem('nickname', nickname.value)
      localStorage.setItem('email', email.value)
      localStorage.setItem('about', about.value)
      localStorage.setItem('avatar', avatar.src)
      window.location.href = './index.html'
    }
  }


  
}

new Registration()
