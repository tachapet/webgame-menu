//* * Trida, ktera vytvori navigacni menu a pripoji do ni take avatara a funkci online/offline */
class HomeApp {
  constructor () {
    this.checkRegistration()
    this.onlineStatus()
    this.addInformation()
    this.pages = document.querySelectorAll('section')
    this.route()
    window.addEventListener('popstate', e => this.route())
  }

  //* * Funkce, ktera zkontroluje jestli byl uzivatel nekdy prihlasen (pokud se nikdy neregistroval, automaticky ho to presune na stranku registration.html) */
  checkRegistration () {
    if (localStorage.length == 0) {
      window.location.href = './registration.html'
    }
  }

  //* *    Funkce, ktera prida do horni listy avatara (obrazek)        */
  addInformation () {
    const avatar = document.querySelector('#img-avatar')
    if (localStorage.getItem('avatar') != '') {
      avatar.setAttribute('src', localStorage.getItem('avatar'))
    } else {
      avatar.setAttribute('src', '../icons/noImage.svg')
    }
  }
  //* * Funkce, ktera kontroluje stav pripojeni k internetu (online,, offline) */
  onlineStatus () {
    const availability = document.querySelector('.availability')

    window.addEventListener('offline', function (e) {
      availability.classList.add('offline')
    })

    window.addEventListener('online', function (e) {
      availability.classList.remove('offline')
    })
  }

  //* * Funkce, ktera zmeni aktualni stranku podle hashe, ktery prijme */
  changePage (section) {
    for (let page of this.pages) {
      if (page.getAttribute('data-route') == section) {
        page.classList.add('is-visible')
      } else {
        page.classList.remove('is-visible')
      }
    }
  }

  //* * Funkce, která na zaklade aktualniho hashe zavola funkci changePage() a nastavi title na odpovidajici stranku */
  route () {
    let hash = window.location.hash
    switch (hash) {
      case '#home':
        this.changePage(hash)
        break
      case '#games':
        this.changePage(hash)
        break
      case '#tutorial':
        this.changePage(hash)
        break
      case '#about':
        this.changePage(hash)
        break
      default:
        this.changePage('#home')
        break
    }

    if (hash == '') {
      window.location.hash = 'home'
    }
    document.title = hash
  }
}

new HomeApp()
