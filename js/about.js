/**
 *
 * funkce informationAboutUserInit vezme vsechny hodnoty z local storage a prida je na misto na strance, pokud uzivatel nenahral avatara (obrazek), zvoli se zakladni obrazek
 *
 */
function informationAboutUserInit () {
  const nicknameDiv = document.querySelector('#nickname_info')
  const emailDiv = document.querySelector('#email_info')
  const aboutSec = document.querySelector('#about_info')
  const avatarSec = document.querySelector('#avatar_info')
  nicknameDiv.innerHTML = localStorage.getItem('nickname')
  emailDiv.innerHTML = localStorage.getItem('email')
  aboutSec.innerHTML = localStorage.getItem('about')

  if (localStorage.getItem('avatar') != '') {
    avatarSec.setAttribute('src', localStorage.getItem('avatar'))
  } else {
    avatarSec.setAttribute('src', '../icons/noImage.svg')
  }
}

informationAboutUserInit()
