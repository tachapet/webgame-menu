//* * Trida pro vztvoreni seznamu her ve webgame menu */
class GamesInit {
  //* * Kontruktor pro referenci na list games a zavolani init funkce   */
  constructor () {
    this.games_list = document.querySelector('#games_list')
    this.init()
  }

  //* * Trida, ktera dostane pres XMLHttpRequest JSON objekt s daty o vytvorenych hrach (jejich umisteni a jejich obrazek), pro napsani teto metody jsem se inspiroval na web. strance StackOverflow */
  loadJSON (callback) {
    var xobj = new XMLHttpRequest()
    xobj.overrideMimeType('application/json')
    xobj.open('GET', '../games_list/games_init.json', true) // Replace 'my_data' with the path to your file
    xobj.onreadystatechange = function () {
      if (xobj.readyState == 4 && xobj.status == '200') {
        // Required use of an anonymous callback as .open will NOT return a value but simply returns undefined in asynchronous mode
        callback(xobj.responseText)
      }
    }
    xobj.send(null)
  }

  //* * Funkce, ktera vytvori ze ziskanych her list, ktery nasledne pripoji do stranky. Nad kazdou hrou se vytvori svg play button*/
  createGridofGames (games_json) {
    games_json['games'].forEach(game => {
      const created_game = document.createElement('li')
      created_game.setAttribute('id', 'game')
      created_game.setAttribute('name', game['name'])
      created_game.setAttribute('class', 'game')
      created_game.style.backgroundImage = `url('${game['icon_path']}')`

      created_game.insertAdjacentHTML('beforeend', '\
      <svg class="game-overlay-play-button" viewBox="0 0 200 200" alt="Play game">\
          <circle cx="100" cy="100" r="90" fill="none" stroke-width="15" stroke="#000"/>\
          <polygon points="70, 55 70, 145 145, 100" fill="#000"/>\
      </svg>\ ')
      
      created_game.addEventListener("click",e => this.startGame(game['path']))

      this.games_list.appendChild(created_game)
    })
  }

  /** Funkce ktera presmeruje stranku podle path */
  startGame(path){
    window.location.href = path;
  }

  //* * init funkce, ktera zavola request na JSON soubor a nasledne zavola funkci, ktera vytvori seznam her */
  init () {
    this.loadJSON(
      function (response) {
        // Parse JSON string into object
        var games_json = JSON.parse(response)
        this.createGridofGames(games_json)
      }.bind(this)
    )
  }



}

new GamesInit()


/* 
https://codepen.io/horne3754sg/pen/KgxRLr

<svg class="video-overlay-play-button" viewBox="0 0 200 200" alt="Play video">\
                    <circle cx="100" cy="100" r="90" fill="none" stroke-width="15" stroke="#fff"/>\
                    <polygon points="70, 55 70, 145 145, 100" fill="#fff"/>\
                </svg> */